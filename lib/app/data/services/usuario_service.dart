import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../model/usuario-model.dart';

class UsuarioService extends GetConnect {
  UsuarioService.internal();
  static UsuarioService instance = UsuarioService.internal();

  Future<List<Usuario>> getUsuarios() async {
    try {
      var response = await get('https://jsonplaceholder.typicode.com/users');
      return response.body
          .map<Usuario>((json) => Usuario.fromMap(json))
          .toList();
    } catch (e) {
      throw Exception(e);
    }
  }
}
