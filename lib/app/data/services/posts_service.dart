import 'dart:convert';
import 'package:ceiba/app/data/model/post-model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../model/usuario-model.dart';

class PostsService extends GetConnect {
  PostsService.internal();
  static PostsService instance = PostsService.internal();

  Future<List<Post>> getPosts(idUsuario) async {
    try {
      var response = await get(
          'https://jsonplaceholder.typicode.com/posts?userId=$idUsuario');
      return response.body.map<Post>((json) => Post.fromMap(json)).toList();
    } catch (e) {
      throw Exception(e);
    }
  }
}
