import 'dart:convert';
import 'dart:developer';

import 'package:ceiba/app/data/model/usuario-model.dart';
import 'package:ceiba/app/data/services/usuario_service.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class UsuarioController extends GetxController {
  var usuarios = new RxList<Usuario>();
  var listaUsuarios = new RxList<Usuario>();
  final storage = GetStorage();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    cargarUsuarios();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  Future<void> cargarUsuarios() async {
    final existeLista = storage.hasData("usuarios");
    if (!existeLista) {
      try {
        usuarios.value = await UsuarioService.instance.getUsuarios();
        listaUsuarios.value = usuarios.value;
        await storage.write("usuarios", usuarios.value);
      } catch (e) {
        print(e);
      }
    } else {
      final lista = storage.read("usuarios");
      usuarios.value =
          lista.map<Usuario>((json) => Usuario.fromMap(json)).toList();
      listaUsuarios.value = usuarios.value;
    }
  }

  void buscar(String texto) {
    texto = texto.toLowerCase();
    usuarios.value = listaUsuarios.where((u) {
      var nombre = u.name!.toLowerCase();
      return nombre.contains(texto);
    }).toList();
    usuarios.refresh();
  }
}
