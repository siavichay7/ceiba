import 'package:ceiba/app/modules/usuario/usuario_controller.dart';
import 'package:get/get.dart';

class UsuarioBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UsuarioController());
  }
}
