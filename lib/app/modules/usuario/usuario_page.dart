import 'package:ceiba/app/modules/usuario/usuario_controller.dart';
import 'package:ceiba/app/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UsuarioPage extends GetWidget<UsuarioController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text("Prueba de ingreso",
              style: TextStyle(fontWeight: FontWeight.bold)),
        ),
        body: Padding(
          padding: const EdgeInsets.all(12.0),
          child: ListView(
            shrinkWrap: true,
            children: [buscador(), listaUsuarios()],
          ),
        ));
  }

  buscador() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: "Buscar Usuario",
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.green, width: 2.0),
          borderRadius: BorderRadius.circular(25.0),
        ),
      ),
      onChanged: (texto) {
        controller.buscar(texto);
      },
    );
  }

  Widget listaUsuarios() {
    return Obx(() => controller.usuarios.length != 0
        ? ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: controller.usuarios.length,
            itemBuilder: (_, int index) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 12.0),
                child: Card(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListTile(
                          title: titulo(
                              controller.usuarios[index].name.toString()),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              telefono(
                                  controller.usuarios[index].phone.toString()),
                              correo(
                                  controller.usuarios[index].email.toString())
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Align(
                          alignment: Alignment.topRight,
                          child: GestureDetector(
                            onTap: () {
                              Get.toNamed(AppRoutes.POST,
                                  arguments: controller.usuarios[index]);
                            },
                            child: Text("VER PUBLICACIONES",
                                style: TextStyle(
                                    color: Colors.green,
                                    fontWeight: FontWeight.bold)),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              );
            })
        : Padding(
            padding: const EdgeInsets.symmetric(vertical: 88.0),
            child: Center(
              child: Text("LIST IS EMPTY",
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold)),
            ),
          ));
  }

  Widget titulo(texto) {
    return Text(texto,
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green));
  }

  Widget telefono(phone) {
    return Row(
      children: [
        Icon(Icons.phone),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Text(phone, style: TextStyle(color: Colors.black)),
        )
      ],
    );
  }

  Widget correo(email) {
    return Row(
      children: [
        Icon(Icons.mail),
        Text(email, style: TextStyle(color: Colors.black))
      ],
    );
  }
}
