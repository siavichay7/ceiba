import 'package:ceiba/app/data/model/post-model.dart';
import 'package:ceiba/app/data/model/usuario-model.dart';
import 'package:ceiba/app/data/services/posts_service.dart';
import 'package:ceiba/app/data/services/usuario_service.dart';
import 'package:get/get.dart';

class PostsController extends GetxController {
  var posts = new RxList<Post>();
  var usuario = Usuario().obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    cargarPosts();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  Future<void> cargarPosts() async {
    try {
      usuario.value = Get.arguments;
      posts.value = await PostsService.instance.getPosts(usuario.value.id);
    } catch (e) {}
  }
}
