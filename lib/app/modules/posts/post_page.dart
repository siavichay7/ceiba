import 'package:ceiba/app/modules/posts/posts_controller.dart';
import 'package:ceiba/app/modules/usuario/usuario_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PostPage extends GetWidget<PostsController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.green,
            title: Obx(
              () => Text("Post " + controller.usuario.value.name.toString(),
                  style: TextStyle(fontWeight: FontWeight.bold)),
            )),
        body: Padding(
          padding: const EdgeInsets.all(12.0),
          child: ListView(
            shrinkWrap: true,
            children: [listaPosts()],
          ),
        ));
  }

  Widget listaPosts() {
    return Obx(() => ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: controller.posts.length,
        itemBuilder: (_, int index) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 12.0),
            child: Card(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListTile(
                      title: titulo(controller.usuario.value.name.toString()),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          telefono(controller.usuario.value.phone.toString()),
                          correo(controller.usuario.value.email.toString())
                        ],
                      ),
                    ),
                  ),
                  posts(controller.posts[index].title.toString(),
                      controller.posts[index].body.toString())
                ],
              ),
            ),
          );
        }));
  }

  Widget titulo(texto) {
    return Text(texto,
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green));
  }

  Widget telefono(phone) {
    return Row(
      children: [
        Icon(Icons.phone),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Text(phone, style: TextStyle(color: Colors.black)),
        )
      ],
    );
  }

  Widget correo(email) {
    return Row(
      children: [
        Icon(Icons.mail),
        Text(email, style: TextStyle(color: Colors.black))
      ],
    );
  }

  Widget posts(titulo, descripcion) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(titulo,
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        SizedBox(
          height: 10,
        ),
        Text(descripcion, style: TextStyle(color: Colors.black))
      ]),
    );
  }
}
