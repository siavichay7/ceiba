import 'package:ceiba/app/modules/posts/posts_controller.dart';
import 'package:ceiba/app/modules/usuario/usuario_controller.dart';
import 'package:get/get.dart';

class PostBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PostsController());
  }
}
