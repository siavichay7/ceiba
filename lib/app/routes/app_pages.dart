import 'package:ceiba/app/modules/posts/post_page.dart';
import 'package:ceiba/app/modules/posts/posts_binding.dart';
import 'package:ceiba/app/modules/usuario/usuario_binding.dart';
import 'package:ceiba/app/modules/usuario/usuario_page.dart';
import 'package:get/get.dart';

import 'app_routes.dart';

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
        name: AppRoutes.USUARIO,
        page: () => UsuarioPage(),
        binding: UsuarioBinding(),
        transition: Transition.fadeIn,
        transitionDuration: Duration(milliseconds: 300)),
    GetPage(
        name: AppRoutes.POST,
        page: () => PostPage(),
        binding: PostBinding(),
        transition: Transition.fadeIn,
        transitionDuration: Duration(milliseconds: 300)),
  ];
}
