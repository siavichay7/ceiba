// @dart=2.9
import 'package:ceiba/app/modules/usuario/usuario_binding.dart';
import 'package:ceiba/app/modules/usuario/usuario_page.dart';
import 'package:ceiba/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

Future<void> main() async {
  await GetStorage.init();
  // Fetch the available cameras before initializing the app.
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(Ceiba());
}

class Ceiba extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Get.put(GlobalController());
    // Get.put(BaseService());
    return GetMaterialApp(
      // localizationsDelegates: [
      //   GlobalMaterialLocalizations.delegate,
      //   GlobalWidgetsLocalizations.delegate,
      //   GlobalCupertinoLocalizations.delegate,
      // ],
      // supportedLocales: [
      //   const Locale('es', 'ES'), // Arabic, no country code
      // ],
      theme: ThemeData(
          fontFamily: 'aero',
          primaryIconTheme: IconThemeData(
            color: Colors.white,
          )),
      debugShowCheckedModeBanner: false,
      title: 'Ceiba',
      home: UsuarioPage(),
      initialBinding: UsuarioBinding(),
      getPages: AppPages.pages,
    );
  }
}
